\babel@toc {russian}{}\relax 
\contentsline {section}{\numberline {1}Введение}{3}{section.1}%
\contentsline {section}{\numberline {2}Матрица}{3}{section.2}%
\contentsline {section}{\numberline {3}Разреженная матрица}{3}{section.3}%
\contentsline {section}{\numberline {4}Схема Дженнингса}{3}{section.4}%
\contentsline {subsection}{\numberline {4.1}Описание}{3}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Алгоритм сжатия матрицы по схеме Дженнингса}{4}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Алгоритм суммы без распаковки двух матриц, упакованных по схеме Дженнингса}{4}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Алгоритм распаковки матрицы, упакованной по схеме Дженнингса}{5}{subsection.4.4}%
\contentsline {section}{\numberline {5}Кольцевая схема Рейнбольдта-Местеньи}{5}{section.5}%
\contentsline {subsection}{\numberline {5.1}Описание}{5}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Алгоритм добавления элемента в массив $an$}{5}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Алгоритм сжатия матриц по кольцевой схеме Рейнбольдта-Местеньи}{6}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Алгоритм определения столбцовой координаты элемента}{6}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}Алгоритм определения строчной координаты элемента}{6}{subsection.5.5}%
\contentsline {subsection}{\numberline {5.6}Алгоритм распаковки матрицы, упакованной по кольцевой схеме Рейнбольдта-Местеньи}{6}{subsection.5.6}%
\contentsline {subsection}{\numberline {5.7}Алгоритм суммы без распаковки двух матриц, упакованных по кольцевой схеме Рейнбольдта-Местеньи}{6}{subsection.5.7}%
\contentsline {subsection}{\numberline {5.8}Алгоритм умножения без распаковки двух матриц, упакованных по кольцевой схеме Рейнбольдта-Местеньи}{7}{subsection.5.8}%
\contentsline {section}{\numberline {6}Реализация алгоритмов}{7}{section.6}%
\contentsline {section}{\numberline {7}Анализ эффективности}{24}{section.7}%
\contentsline {section}{\numberline {8}Тесты}{25}{section.8}%
\contentsline {subsection}{\numberline {8.1}Схема Дженнингса}{25}{subsection.8.1}%
\contentsline {subsection}{\numberline {8.2}Кольцевая схема Рейнбольдта-Местеньи, сложение матриц}{31}{subsection.8.2}%
\contentsline {subsection}{\numberline {8.3}Кольцевая схема Рейнбольдта-Местеньи, умножение матриц}{37}{subsection.8.3}%
\contentsline {section}{\numberline {9}Примеры работы}{43}{section.9}%
\contentsline {subsection}{\numberline {9.1}Схема Дженнингса}{43}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Кольцевая схема Рейнбольдта-Местеньи}{45}{subsection.9.2}%
\contentsline {section}{\numberline {10}Заключение}{49}{section.10}%
\contentsline {section}{Список используемых источников}{50}{section*.27}%
